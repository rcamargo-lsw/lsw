package lsw

import ( 
	"fmt"
	"time"
	"os"
)

var Arg0 = ""

func Syslg_arg0(module string) {
	Arg0 = module
}

func Get_arg0() string {
	return Arg0
}

func Syslgsend( msg string ) {

	fmt.Println("%s %s(%d) %s\n", time.Now(), Arg0,  os.Getpid(), msg )
}
 